# Detach disk
`virsh detach-disk`

# Add disk
`virsh attach-disk {vm-name} /var/lib/libvirt/images/{img-name-here} vdb --cache none`